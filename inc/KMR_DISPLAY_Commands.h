/*
 * KMR_DISPLAY_Commands.h
 *
 * MIT License
 *
 * Copyright (c) 2022 Vandevelde Niel
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef INC_KMR_DISPLAY_COMMANDS_H_
#define INC_KMR_DISPLAY_COMMANDS_H_

#define DISPLAY_CMD_NOP                     0x00
#define DISPLAY_CMD_SOFT_RESET              0x01
#define DISPLAY_CMD_GET_RED_CHANNEL         0x06
#define DISPLAY_CMD_GET_GREEN_CHANNEL       0x07
#define DISPLAY_CMD_GET_BLUE_CHANNEL        0x08
#define DISPLAY_CMD_GET_PIXEL_FORMAT        0x0C
#define DISPLAY_CMD_GET_POWER_MODE          0x0A
#define DISPLAY_CMD_GET_ADDRESS_MODE        0x0B
#define DISPLAY_CMD_GET_DISPLAY_MODE        0x0D
#define DISPLAY_CMD_GET_SIGNAL_MODE         0x0E
#define DISPLAY_CMD_GET_DIAGNOSTIC_RESULT   0x0F
#define DISPLAY_CMD_ENTER_SLEEP_MODE        0x10
#define DISPLAY_CMD_EXIT_SLEEP_MODE         0x11
#define DISPLAY_CMD_ENTER_PARTIAL_MODE      0x12
#define DISPLAY_CMD_ENTER_NORMAL_MODE       0x13
#define DISPLAY_CMD_EXIT_INVERT_MODE        0x20
#define DISPLAY_CMD_ENTER_INVERT_MODE       0x21
#define DISPLAY_CMD_SET_GAMMA_CURVE         0x26
#define DISPLAY_CMD_SET_DISPLAY_OFF         0x28
#define DISPLAY_CMD_SET_DISPLAY_ON          0x29
#define DISPLAY_CMD_SET_COLUMN_ADDRESS      0x2A
#define DISPLAY_CMD_SET_PAGE_ADDRESS        0x2B
#define DISPLAY_CMD_WRITE_MEMORY_START      0x2C
#define DISPLAY_CMD_WRITE_LUT               0x2D
#define DISPLAY_CMD_READ_MEMORY_START       0x2E
#define DISPLAY_CMD_SET_PARTIAL_AREA        0x30
#define DISPLAY_CMD_SET_SCROLL_AREA         0x33
#define DISPLAY_CMD_SET_TEAR_OFF            0x34
#define DISPLAY_CMD_SET_TEAR_ON             0x35
#define DISPLAY_CMD_SET_ADDRESS_MODE        0x36
#define DISPLAY_CMD_SET_SCROLL_START        0X37
#define DISPLAY_CMD_EXIT_IDLE_MODE          0x38
#define DISPLAY_CMD_ENTER_IDLE_MODE         0x39
#define DISPLAY_CMD_SET_PIXEL_FORMAT        0x3A
#define DISPLAY_CMD_WRITE_MEMORY_CONTINUE   0x3C
#define DISPLAY_CMD_READ_MEMORY_CONTINUE    0x3E
#define DISPLAY_CMD_SET_TEAR_SCANLINE       0x44
#define DISPLAY_CMD_GET_SCANLINE            0x45
#define DISPLAY_CMD_READ_ID1                0xDA
#define DISPLAY_CMD_READ_ID2                0xDB
#define DISPLAY_CMD_READ_ID3                0xDC
#define DISPLAY_CMD_FRAME_RATE_CONTROL1     0xB1
#define DISPLAY_CMD_FRAME_RATE_CONTROL2     0xB2
#define DISPLAY_CMD_FRAME_RATE_CONTROL3     0xB3
#define DISPLAY_CMD_DISPLAY_INVERSION       0xB4
#define DISPLAY_CMD_SOURCE_DRIVER_DIRECTION 0xB7
#define DISPLAY_CMD_GATE_DRIVER_DIRECTION   0xB8
#define DISPLAY_CMD_POWER_CONTROL1          0xC0
#define DISPLAY_CMD_POWER_CONTROL2          0xC1
#define DISPLAY_CMD_POWER_CONTROL3          0xC2
#define DISPLAY_CMD_POWER_CONTROL4          0xC3
#define DISPLAY_CMD_POWER_CONTROL5          0xC4
#define DISPLAY_CMD_VCOM_CONTROL1           0xC5
#define DISPLAY_CMD_VCOM_CONTROL2           0xC6
#define DISPLAY_CMD_VCOM_OFFSET_CONTROL     0xC7
#define DISPLAY_CMD_WRITE_ID4_VALUE         0xD3
#define DISPLAY_CMD_NV_MEMORY_FUNCTION1     0xD7
#define DISPLAY_CMD_NV_MEMORY_FUNCTION2     0xDE
#define DISPLAY_CMD_POSITIVE_GAMMA_CORRECT  0xE0
#define DISPLAY_CMD_NEGATIVE_GAMMA_CORRECT  0xE1
#define DISPLAY_CMD_GAM_R_SEL               0xF2


#endif /* INC_KMR_DISPLAY_COMMANDS_H_ */
