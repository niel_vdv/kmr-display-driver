/*
 * KMR_DISPLAY_Driver.h
 *
 * MIT License
 *
 * Copyright (c) 2022 Vandevelde Niel
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

/*
 * Pin Configuration:
 *
 * LED- 	-> 		Ground
 * LED +  	-> 		3v3
 * MOSI 	-> 		NC
 * MISO 	-> 		NC
 * SCK 		-> 		NC
 * CS 		-> 		SPI_CS
 * SCL 		-> 		SPI_CLK
 * SDA 		-> 		SPI_MOSI
 * A0(DC)	-> 		GPIO_OUTPUT
 * RESET 	-> 		GPIO_OUTPUT
 * VCC 		-> 		3V3
 * GND 		-> 		Ground
 *
 */

#ifndef INC_KMR_DISPLAY_DRIVER_H_
#define INC_KMR_DISPLAY_DRIVER_H_

#include "main.h"
#include "KMR_DISPLAY_Commands.h"
#include "spi.h"

/****************************************BEGIN IMPLEMENTATION FOR USER********************************/

/**************************/
/*		DRIVER IC		  */
/**************************/
#define ILI9163			0
#define ST7735S			1
/**************************/
/**************************/

/******************************/
/*		DISPLAY SIZE		  */
/******************************/
#define DISPLAY_HEIGHT 	160
#define DISPLAY_WIDTH	128
/******************************/
/******************************/


/******************************/
/*			PIN SETUP		  */
/******************************/
#define DC_PIN			DC_Pin
#define DC_PORT			DC_GPIO_Port

#define RST_PIN			RESET_Pin
#define RST_PORT		RESET_GPIO_Port
/*******************************/
/*******************************/


/******************************/
/*			  SPI			  */
/******************************/
#define DISPLAY_SPI		hspi4
/*******************************/
/*******************************/

/****************************************END IMPLEMENTATION FOR USER********************************/

#define GPIO_LOW		0
#define GPIO_HIGH		1

extern SPI_HandleTypeDef DISP_SPI;

typedef enum{
	NO_ROTATION = 0,
	ROTATION_90_DEGREE = 96,
	ROTATION_180_DEGREE = 160,
	ROTATION_270_DEGREE = 192,
}KMR_DISP_Rotation;


uint8_t KMR_DISP_Init(KMR_DISP_Rotation rotation);
uint8_t KMR_DISP_Fill(uint16_t color);
uint8_t KMR_DISP_Bitmap(uint16_t *bitmap);
uint8_t KMR_DISP_PartialFill(uint8_t x1, uint8_t x2, uint8_t y1, uint8_t y2, uint16_t *bitmap, uint32_t size);
uint8_t KMR_DISP_BlankingOn(void);
uint8_t KMR_DISP_BlankingOff(void);
uint8_t KMPR_DISP_SetPixel(uint8_t x, uint8_t y, uint16_t pixel);



#endif /* INC_KMR_DISPLAY_DRIVER_H_ */
