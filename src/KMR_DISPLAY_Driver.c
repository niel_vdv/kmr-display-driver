/*
 * KMR_DISPLAY_Driver.c
 *
 * MIT License
 *
 * Copyright (c) 2022 Vandevelde Niel
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

/******************************************BEGIN INCLUDES********************************************************/
#include "KMR_DISPLAY_Driver.h"
/*******************************************END INCLUDES*********************************************************/

/*		User Code Implementation for other devices than STM32
 * 		+ void display_delay(uint32_t delay_ms) -> Implement delay in milliseconds
 *   	+ uint8_t spi_write(uint8_t *data, uint32_t size) -> Implement SPI write functions
 *   	+ uint8_t pin_ctrl(GPIO_TypeDef *port, uint16_t pin, uint16_t value) -> Implement GPIO pin control
 */
/******************************************BEGIN USER CODE********************************************************/

/**
 * @brief Delay function for the driver
 * @param delay_ms delay time in milliseconds
 */
static void display_delay(uint32_t delay_ms)
{
	HAL_Delay(delay_ms);
}

/**
 * @brief SPI write function for the driver
 * @param data pointer to data to send
 * @param size size of the data array
 * @return 0 tx OK
 * 		   other tx FAILED
 */
static uint8_t spi_write(uint8_t *data, uint32_t size)
{
	HAL_StatusTypeDef ret;
	ret = HAL_SPI_Transmit(&DISPLAY_SPI, data, size, HAL_MAX_DELAY);
	return (uint8_t) ret;
}

/**
 * @brief GPIO pin control
 * @param port GPIO port to be use
 * @param pin GPIO pin to be used
 * @param value 0 GPIO low
 * 				1 GPIO high
 * @return 0 GPIO set OK
 * 		   other GPIO set FAILED
 */
static uint8_t pin_ctrl(GPIO_TypeDef *port, uint16_t pin, uint16_t value)
{
	HAL_GPIO_WritePin(port, pin, value);
	return 0;
}

/******************************************END USER CODE********************************************************/


/******************************************BEGIN STATIC FUNCTIONS********************************************************/

/**
 * @brief Reset of the driver IC with the reset pin
 * @return 0 reset OK
 * 		   other reset Failed
 */
static uint8_t _KMR_DISP_HWReset()
{
	uint8_t ret = 0;
	ret = pin_ctrl(RST_PORT, RST_PIN, GPIO_LOW);
	if(ret != 0)
		return ret;
	display_delay(500);
	ret = pin_ctrl(RST_PORT, RST_PIN, GPIO_HIGH);
	if(ret != 0)
		return ret;
	display_delay(500);
	return ret;
}

/*
 * @brief Write a command to the display driver
 * @param cmd Command to write to the display
 * @return 0 cmd send OK
 * 		   other cmd send FAIL
 */
static uint8_t _KMR_DISP_WriteCommand(uint8_t cmd)
{
	uint8_t ret = 0;
	ret = pin_ctrl(DC_PORT, DC_PIN, GPIO_LOW);
	if(ret != 0)
		return ret;

	ret = spi_write(&cmd, 1);
	if(ret != 0)
		return ret;

	return ret;
}

/*
 * @brief Write data to the display driver
 * @param data Array of data to be send
 * @param size Size of the data array
 * @return 0 data send OK
 * 		   other data send FAIL
 */
static uint8_t _KMR_DISP_WriteData(uint8_t *data, uint32_t size)
{
	uint8_t ret = 0;
	ret = pin_ctrl(DC_PORT, DC_PIN, GPIO_HIGH);
	if(ret != 0)
		return ret;

	ret = spi_write(data, size);
	if(ret != 0)
		return ret;

	return ret;
}

/*
 * @brief Write a command and data to the display driver
 * @param cmd Command to write to the display driver, if 0 no command send
 * @param data Data array to be send to the display driver, if NULL no data send
 * @param size Size of data array, if 0 no data send
 * @return 0 send OK
 * 		   other send FAIL
 */
static uint8_t _KMR_DISP_Write(uint8_t cmd, uint8_t *data, uint32_t size)
{
	uint8_t ret = 0;
	if(cmd != 0)
	{
		ret = _KMR_DISP_WriteCommand(cmd);
		if(ret != 0)
			return ret;
	}
	if(size != 0 && data != NULL)
	{
		ret = _KMR_DISP_WriteData(data, size);
		if(ret != 0)
			return ret;
	}
	return ret;
}

/*
 * @brief Set the col and row for the internal RAM of the driver IC
 * @param x1 First horizontal location to start writing in the internal RAM, x1 < x2
 * @param x2 End horizontal location to stop writing in the internal RAM, x2 > x1
 * @param y1 Fist vertical location to start writing in the internal RAM, y1 < y2
 * @param y2 End horizontal location to stop wrting in the interanl RAM, y2 > y1
 * @return 0 settings OK
 * 		   100 x1>x2 or y1>y2
 * 		   101 x2 >= DISPLAY_WIDTH
 * 		   102 y2 >= DISPLAY_HEIGHT
 * 		   other settings FAIL
 */
static uint8_t _KMR_DISP_SetAddress(uint8_t x1, uint8_t x2, uint8_t y1, uint8_t y2)
{
	if(x1 > x2 || y1 > y2)
		return 100;
	if(x2 >= DISPLAY_WIDTH)
		return 101;
	if(y2 >= DISPLAY_HEIGHT)
		return 102;

	uint8_t ret = 0;
	uint8_t colAdrData[] = {0x00, x1, 0x00, x2};
	ret = _KMR_DISP_Write(DISPLAY_CMD_SET_COLUMN_ADDRESS, colAdrData, sizeof(colAdrData));
	if(ret != 0)
		return ret;

	uint8_t rowAdrData[] = {0x00, y1, 0x00, y2};
	ret = _KMR_DISP_Write(DISPLAY_CMD_SET_PAGE_ADDRESS, rowAdrData, sizeof(rowAdrData));
	if(ret != 0)
		return ret;

	ret = _KMR_DISP_Write(DISPLAY_CMD_WRITE_MEMORY_START, NULL, 0);
	if(ret != 0)
		return ret;

	return ret;

}

/*
 * @brief Initialization of the driver IC of the display
 * @param rotation Rotation of the screen
 * @return 0 init OK
 * 		   other init FAIL
 */
static uint8_t _KMR_DISP_DriverInit(KMR_DISP_Rotation rotation)
{
	uint8_t ret = 0;

	ret = _KMR_DISP_Write(DISPLAY_CMD_SOFT_RESET, NULL, 0);
	if(ret != 0)
		return ret;

	display_delay(500);

	ret = _KMR_DISP_Write(DISPLAY_CMD_EXIT_SLEEP_MODE, NULL, 0);
	if(ret != 0)
		return ret;

	uint8_t pixelFormatData[1] = {0x05};
	ret = _KMR_DISP_Write(DISPLAY_CMD_SET_PIXEL_FORMAT, pixelFormatData, sizeof(pixelFormatData));
	if(ret != 0)
		return ret;

	uint8_t gammaCurveData[1] = {0x04};
	ret = _KMR_DISP_Write(DISPLAY_CMD_SET_GAMMA_CURVE, gammaCurveData, sizeof(gammaCurveData));
	if(ret != 0)
		return ret;

#if ILI9163
	uint8_t gammaAdjData[1] = {0x01};
	ret = _KMR_DISP_Write(DISPLAY_CMD_GAM_R_SEL, gammaAdjData, sizeof(gammaAdjData));
	if(ret != 0)
		return ret;
#endif

	uint8_t gammaPData[] = {0x3F, 0x25, 0x1C, 0x1E, 0x20, 0x12, 0x2A, 0x90, 0x24, 0x11, 0x00, 0x00, 0x00, 0x00, 0x00};
	ret = _KMR_DISP_Write(DISPLAY_CMD_POSITIVE_GAMMA_CORRECT, gammaPData, sizeof(gammaPData));
	if(ret != 0)
		return ret;

	uint8_t gammaNData[] = {0x3F, 0x25, 0x1C, 0x1E, 0x20, 0x12, 0x2A, 0x90, 0x24, 0x11, 0x00, 0x00, 0x00, 0x00, 0x00};
	ret = _KMR_DISP_Write(DISPLAY_CMD_NEGATIVE_GAMMA_CORRECT, gammaNData, sizeof(gammaNData));
	if(ret != 0)
		return ret;

#if ILI9163
	uint8_t frameCtrlData[] = {0x08, 0x08};
	ret = _KMR_DISP_Write(DISPLAY_CMD_FRAME_RATE_CONTROL1, frameCtrlData, sizeof(frameCtrlData));
	if(ret != 0)
		return ret;
#endif

#if ST7735S
	uint8_t frameCtrlData[] = {0x05, 0x3C, 0x3C};
	ret = _KMR_DISP_Write(DISPLAY_CMD_FRAME_RATE_CONTROL1, frameCtrlData, sizeof(frameCtrlData));
	if(ret != 0)
		return ret;
#endif

	uint8_t invData[] = {0x07};
	ret = _KMR_DISP_Write(DISPLAY_CMD_DISPLAY_INVERSION, invData, sizeof(invData));
	if(ret != 0)
		return ret;

#if ILI9163
	uint8_t pwrCtrl1Data[] = {0x0A, 0x02};
	ret = _KMR_DISP_Write(DISPLAY_CMD_POWER_CONTROL1, pwrCtrl1Data, sizeof(pwrCtrl1Data));
	if(ret != 0)
		return ret;
#endif
#if ST7735S
	uint8_t pwrCtrl1Data[] = {0xFC, 0x08, 0x02};
	ret = _KMR_DISP_Write(DISPLAY_CMD_POWER_CONTROL1, pwrCtrl1Data, sizeof(pwrCtrl1Data));
	if(ret != 0)
		return ret;
#endif


#if ILI9163
	uint8_t pwrCtrl2Data[] = {0x02};
#endif
#if ST7735S
	uint8_t pwrCtrl2Data[] = {0xC0};
#endif
	ret = _KMR_DISP_Write(DISPLAY_CMD_POWER_CONTROL2, pwrCtrl2Data, sizeof(pwrCtrl2Data));
	if(ret != 0)
		return ret;

#if ILI9193
	uint8_t vcomCtrl1Data[] = {0x50, 0x5B};
#endif
#if ST7735S
	uint8_t vcomCtrl1Data[] = {0x0F};
#endif
	ret = _KMR_DISP_Write(DISPLAY_CMD_VCOM_CONTROL1, vcomCtrl1Data, sizeof(vcomCtrl1Data));
	if(ret != 0)
		return ret;

	uint8_t vcomOffsetData[] = {0x40};
	ret = _KMR_DISP_Write(DISPLAY_CMD_VCOM_OFFSET_CONTROL, vcomOffsetData, sizeof(vcomOffsetData));
	if(ret != 0)
		return ret;

	uint8_t colAdrData[] = {0x00, 0x00, 0x00, DISPLAY_WIDTH-1};
	ret = _KMR_DISP_Write(DISPLAY_CMD_SET_COLUMN_ADDRESS, colAdrData, sizeof(colAdrData));
	if(ret != 0)
		return ret;

	uint8_t rowAdrData[] = {0x00, 0x00, 0x00, DISPLAY_HEIGHT-1};
	ret = _KMR_DISP_Write(DISPLAY_CMD_SET_PAGE_ADDRESS, rowAdrData, sizeof(rowAdrData));
	if(ret != 0)
		return ret;

	uint8_t madctlData[] = {(uint8_t)rotation};
	ret = _KMR_DISP_Write(DISPLAY_CMD_SET_ADDRESS_MODE, madctlData, sizeof(madctlData));
	if(ret != 0)
		return ret;

	ret = _KMR_DISP_Write(DISPLAY_CMD_ENTER_NORMAL_MODE, NULL, 0);
	if(ret != 0)
			return ret;

	ret = _KMR_DISP_Write(DISPLAY_CMD_SET_DISPLAY_ON, NULL, 0);
	if(ret != 0)
		return ret;

	ret = _KMR_DISP_Write(DISPLAY_CMD_WRITE_MEMORY_START, NULL, 0);
	if(ret != 0)
		return ret;


	return ret;
}

/******************************************END STATIC FUNCTIONS********************************************************/

/******************************************BEGIN PUBLIC FUNCTIONS********************************************************/

/*
 * @brief Initialization of the display
 * @param rotation Rotation of the display
 * @return 0 init OK
 * 		   1 init FAIL
 */
uint8_t KMR_DISP_Init(KMR_DISP_Rotation rotation)
{
	uint8_t ret = 0;

	ret = _KMR_DISP_HWReset();
	if(ret != 0)
		return ret;

	ret = _KMR_DISP_DriverInit(rotation);
	if(ret != 0)
		return ret;

	return ret;

}

/*
 * @brief Set the display to a given color (RGB565)
 * @param color Color to show on the display
 * @return 0 fill OK
 * 		   other fill FAIL
 */
uint8_t KMR_DISP_Fill(uint16_t color)
{
	uint8_t ret = 0;
	ret = _KMR_DISP_SetAddress(0, DISPLAY_WIDTH-1, 0, DISPLAY_HEIGHT-1);
	if(ret != 0)
		return ret;

	uint8_t data[2];
	data[0] = color >> 8 & 0xff;
	data[1] = color & 0xff;

	for(uint8_t x = 0; x < DISPLAY_WIDTH; x++)
	{
		for(uint8_t y = 0; y < DISPLAY_HEIGHT; y++)
		{
			ret = _KMR_DISP_Write(0, data, 2);
			if(ret != 0)
					return ret;
		}
	}
	return ret;
}

/*
 * @brief Set the display to a given bitmap (RGB565)
 * @param bitmap Array the size of the screen to show on the display
 * @param size Size of the bitmap array
 * @return 0 fill OK
 * 		   other fill FAIL
 */
uint8_t KMR_DISP_Bitmap(uint16_t *bitmap)
{
	uint8_t ret = 0;
	ret = _KMR_DISP_SetAddress(0, DISPLAY_WIDTH-1, 0, DISPLAY_HEIGHT-1);
	if(ret != 0)
		return ret;

	uint8_t data[2];

	for(uint32_t i = 0; i < DISPLAY_WIDTH*DISPLAY_HEIGHT; i++)
	{
		data[0] = bitmap[i] >> 8 & 0xFF;
		data[1] = bitmap[i] & 0xFF;
		ret = _KMR_DISP_WriteData(data, 2);
		if(ret != 0)
			return ret;
	}


	return ret;
}


/*
 * @brief Blanking on of the display
 * @return 0 blanking OK
 * 		   other blanking FAIL
 */
uint8_t KMR_DISP_BlankingOn(void)
{
	return _KMR_DISP_Write(DISPLAY_CMD_SET_DISPLAY_OFF, NULL, 0);
}

/*
 * @brief Blanking off of the display
 * @return 0 blanking off OK
 * 		   other blanking off FAIL
 */
uint8_t KMR_DISP_BlankingOff(void)
{
	return _KMR_DISP_Write(DISPLAY_CMD_SET_DISPLAY_ON, NULL, 0);
}

/*
 * @brief Set the col and row for the internal RAM of the driver IC
 * @param x1 First horizontal location to start writing in the internal RAM, x1 < x2
 * @param x2 End horizontal location to stop writing in the internal RAM, x2 > x1 & x2 =< DISPLAY_WIDTH-1
 * @param y1 Fist vertical location to start writing in the internal RAM, y1 < y2
 * @param y2 End horizontal location to stop wrting in the interanl RAM, y2 > y1 & y2 =< DISPLAY_HEIGHT-1
 * @param bitmap Array of data the fill the given area
 * @param size Size of the bitmap array
 * @return 0 settings OK
 * 		   100 x1>x2 or y1>y2
 * 		   101 x2 >= DISPLAY_WIDTH
 * 		   102 y2 >= DISPLAY_HEIGHT
 * 		   other settings FAIL
 */
uint8_t KMR_DISP_PartialFill(uint8_t x1, uint8_t x2, uint8_t y1, uint8_t y2, uint16_t *bitmap, uint32_t size)
{
	uint8_t ret = 0;
	ret = _KMR_DISP_SetAddress(x1, x2, y1, y2);
	if(ret != 0)
		return ret;

	ret = _KMR_DISP_Write(DISPLAY_CMD_WRITE_MEMORY_START, NULL, 0);
	if(ret != 0)
		return ret;
	uint8_t data[2];
	for(uint32_t i = 0; i < size; i++)
	{
		data[0] = bitmap[i] >> 8 & 0xFF;
		data[1] = bitmap[i] & 0xFF;
		ret = _KMR_DISP_Write(0, data, 2);
			if(ret != 0)
				return ret;
	}
	return ret;
}

/*
 * @brief Set a pixel on the screen
 * @param x Horizontal location of the pixel
 * @param y Vertical location of the pixel
 * @param pixel Color of the pixel to set
 * @return 0 settings OK
 * 		   other settings FAIL
 */
uint8_t KMPR_DISP_SetPixel(uint8_t x, uint8_t y, uint16_t pixel)
{
	uint8_t ret = 0;
	ret = _KMR_DISP_SetAddress(x, x, y, y);
		if(ret != 0)
			return ret;

	ret = _KMR_DISP_Write(DISPLAY_CMD_WRITE_MEMORY_START, NULL, 0);
		if(ret != 0)
			return ret;

	uint8_t data[2];
	data[0] = pixel >> 8 & 0xFF;
	data[1] = pixel & 0xFF;

	ret = _KMR_DISP_Write(0, data, 2);
	if(ret != 0)
		return ret;

	return ret;
}


/******************************************END PUBLIC FUNCTIONS********************************************************/



