# Display driver for ILI9163 and ST7735S
## Introduction
This driver was designed for a KMR-1.8 SPI display. It's using STM32 HAL for sending SPI data to the display, but it shouldn't be a lot of work to port the driver to whatever MCU you like. The functions that should be ported are descriped in the .c file of the driver. The driver supports ILI9163 and ST7735S. Modify the .h file to select which screen driver you have. The command values are almost the same for ILI9136, ST7735S and  S6D02A1. 

## How to use
### 1. Configuration
In KMR_DISPLAY_Driver.h select which driver IC is used in the display.
``` 
#define ILI9163			0
#define ST7735S			1
``` 
Set the number of pixels of the screen. This is depending on the GM1, GM0 pins of the driver.
``` 
#define DISPLAY_HEIGHT 	160
#define DISPLAY_WIDTH	128
``` 
Next is setting up the other GPIO pins. Th DC pin is used to let the drive IC now whether the send bytes are a command or are data. 
``` 
#define DC_PIN			DC_Pin
#define DC_PORT			DC_GPIO_Port

#define RST_PIN			RESET_Pin
#define RST_PORT		RESET_GPIO_Port
``` 
The last thing for STM32 MCU's is selecting which SPI handle the HAL should use. 
``` 
#define DISPLAY_SPI		hspi4
``` 
### 2. Include
Include the KMR_DISPLAY_Driver.h file to add the driver. The KMR_DISPLAY_Colors.h only has be added if RGB565 colors are needed. 
``` 
#include "KMR_DISPLAY_Driver.h"
#include "KMR_DISPLAY_Colors.h"
``` 

### 3. Initialize
Init of the display can be done by calling this function. If the init is done correctly, this function should return 0. 
``` 
uint8_t KMR_DISP_Init(KMR_DISP_Rotation rotation);
``` 
The rotation parameter is an enum given by :
``` 
typedef enum{
	NO_ROTATION = 0,
	ROTATION_90_DEGREE = 96,
	ROTATION_180_DEGREE = 160,
	ROTATION_270_DEGREE = 192,
}KMR_DISP_Rotation;
``` 
The blanking is turned of after the init. It can be turned on again with these functions.
``` 
uint8_t KMR_DISP_BlankingOn(void);
uint8_t KMR_DISP_BlankingOff(void);
``` 

### 4. Screen control
The following functions can be used to fill the internal RAM of the display. The names of the function are explaining the usage of these functions. 
``` 
uint8_t KMR_DISP_Fill(uint16_t color);
uint8_t KMR_DISP_Bitmap(uint16_t *bitmap);
uint8_t KMR_DISP_PartialFill(uint8_t x1, uint8_t x2, uint8_t y1, uint8_t y2, uint16_t *bitmap, uint32_t size);
``` 
## Tested
[x] No graphical engine
[x] LVGL
[ ] TGFX

